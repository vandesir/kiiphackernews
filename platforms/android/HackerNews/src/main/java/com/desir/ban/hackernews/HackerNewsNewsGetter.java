package com.desir.ban.hackernews;

import com.desir.ban.hackernews.RetrofitHelper.HackerNewsRetroInterface;
import com.desir.ban.hackernews.model.News;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by bandeshor on 3/17/2017.
 */

public class HackerNewsNewsGetter implements Callback<News> {

    private int count = 0;
    private int topX = 0;
    private List<News> newsList;
    private NewsGetterInterface newsGetterInterface;

    public HackerNewsNewsGetter(NewsGetterInterface newsGetterInterface){
        newsList= new ArrayList<News>();
        this.newsGetterInterface = newsGetterInterface;
    }

    public void getTopXNews(HackerNewsRetroInterface hackerNewsRetroInterface,String[] ids,int topX){

        checkAndSetIfTopXGreaterThanNumberOfNewsIds(ids,topX);

        for(int i=0;i<topX;i++){
            hackerNewsRetroInterface.getNewsFromId(ids[i]).enqueue(this);
        }
    }

    private void checkAndSetIfTopXGreaterThanNumberOfNewsIds(String[] ids,int topX){
        topX = ids.length >= topX? ids.length:topX;//if numbers of Ids returned is smaller than topX, set topX and number of Ids
        this.topX = topX;
    }

    //increase count on response or faliure so that we know when to return the results
    private void increaseCount(){
        count++;
    }

    private void checkIfGotAllNeededAndReturnList(){
        if(count == topX){
            if(newsGetterInterface!=null)
                newsGetterInterface.gotAllNeeded(newsList);
        }
    }

    @Override
    public void onResponse(Call<News> call, Response<News> response) {
        newsList.add(response.body());
        increaseCount();
        checkIfGotAllNeededAndReturnList();
    }

    @Override
    public void onFailure(Call<News> call, Throwable t) {
        increaseCount();
        checkIfGotAllNeededAndReturnList();
    }

    public interface NewsGetterInterface{
        void gotAllNeeded(List<News> newsList);
    }

}
