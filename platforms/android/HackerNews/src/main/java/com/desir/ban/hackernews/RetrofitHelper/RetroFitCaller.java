package com.desir.ban.hackernews.RetrofitHelper;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by bandeshor on 3/17/2017.
 */

public class RetroFitCaller {

    private Retrofit mRetroFit;
    private HackerNewsRetroInterface mHackerNewsRetroInterface;

    public RetroFitCaller(String baseURL){
        mRetroFit = new Retrofit.Builder()
                .baseUrl(baseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mHackerNewsRetroInterface = mRetroFit.create(HackerNewsRetroInterface.class);
    }

    public  HackerNewsRetroInterface getRestInterface(){
        return  mHackerNewsRetroInterface;
    }
}
