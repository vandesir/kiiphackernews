package com.desir.ban.hackernews;

import com.desir.ban.hackernews.RetrofitHelper.HackerNewsRetroInterface;
import com.desir.ban.hackernews.RetrofitHelper.RetroFitCaller;
import com.desir.ban.hackernews.model.News;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by bandeshor on 3/17/2017.
 */

public class HackerNewsAPI implements HackerNewsNewsGetter.NewsGetterInterface{

    private String BASE_URL = "https://hacker-news.firebaseio.com";
    private HackerNewsRetroInterface mHackerNewsRetroInterface = null;
    private HackerNewsAPIInterface hackerNewsAPIInterface;

    public void getTopNewsByNumber(int topX){
        RetroFitCaller retroFitCaller = new RetroFitCaller(BASE_URL);
        mHackerNewsRetroInterface = retroFitCaller.getRestInterface();
        getTopXNewsId(topX);
    }

    private void getTopXNewsId(final int topX){
        Call<ResponseBody> topNewsId =mHackerNewsRetroInterface.getTopNewsId();
        topNewsId.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String array = call.toString();
                convertAndReturnTopNews(array,topX);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void getListofTopXNews(int topX,String[] newsIds){
        HackerNewsNewsGetter hackerNewsNewsGetter = new HackerNewsNewsGetter(this);
        hackerNewsNewsGetter.getTopXNews(mHackerNewsRetroInterface,newsIds,topX);
    }

    private void convertAndReturnTopNews(String arrayString,int topX){
        arrayString = arrayString.replace("[","");
        arrayString = arrayString.trim();
        String[] ids = arrayString.split(",");
        getListofTopXNews(topX,ids);
    }

    @Override
    public void gotAllNeeded(List<News> newsList) {
        hackerNewsAPIInterface.gotListTopXNews(newsList);
    }

    public interface HackerNewsAPIInterface{
        void gotListTopXNews(List<News> newsListws);
    }
}
