package com.desir.ban.hackernews.RetrofitHelper;

import com.desir.ban.hackernews.model.News;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by bandeshor on 3/17/2017.
 */

public interface HackerNewsRetroInterface {

    @GET("/v0/topstories.json")
    public Call<ResponseBody> getTopNewsId();

    @GET("/v0/item/{newsId}.json")
    public Call<News> getNewsFromId(@Path("newsId") String newsId);
}
