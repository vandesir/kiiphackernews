package com.ban.kiip.hackernews;

import com.desir.ban.hackernewsapi.HackerNewsSDK;
import com.desir.ban.hackernewsapi.model.News;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

/**
 * Created by bandeshor on 3/18/2017.
 */

public class HackerNewsSDKCordovaPlugin extends CordovaPlugin {
    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
        if(action.equals("getTop20News")){
            HackerNewsSDK sdk = new HackerNewsSDK();
            sdk.initSDK(new HackerNewsSDK.HackerNewsSDkInterface() {
                @Override
                public void gotTop20News(List<News> newsList) {
                    JSONArray array = returnJSONObjectForArrayOfNews(newsList);
                    callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK,array));
                }
            });
            sdk.getTop20News();
            return true;
        }
        return false;
    }

    private JSONArray returnJSONObjectForArrayOfNews(List<News> newsList){
        Gson gson = new GsonBuilder().create();
        String arString = gson.toJson(newsList);
        JSONArray array=new JSONArray();
        try {
            array = new JSONArray(arString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return array;
    }
}
