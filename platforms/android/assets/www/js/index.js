/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    onDeviceReady: function() {
        this.getTop20();
        $("#loadingP").html("Loading Data...");
        $(document.body).on('click', '.clickable' ,function(){
         //now show the loading with changed message and hide the dataDiv
            $("#loadingP").html("Going to the news...");
            $("#loading").show();
            $("#dataDiv").hide();

            var url = $(this).attr("theUrl");
            window.open(url, '_blank');
        });

        //
    },

    getTop20:function(){
     var callBackFunction = function(array){
                      var data = jQuery.parseJSON(JSON.stringify(array));
                      var pStyle="style = 'margin: 0px;font-size: 12px;'";
                      var hStyle = "style = 'margin: 0px;white-space: normal;'";
                      $.each(data, function(i, news) {
                            myDate = new Date(1000*news.time);
                            $("#tableHere").append(
                              '<li ><div theUrl="'+news.url+'" class="ui-btn ui-btn-icon-right ui-icon-carat-r clickable" style="text-align: left;color: black;"'+
                              '>'+
                               '<h2 '+hStyle+'>'+news.title+'</h2>'+
                               '<p '+pStyle+'>Score : '+news.score+'</p>'+
                               '<p '+pStyle+'>Type : '+news.type+'</p>'+
                               '<p '+pStyle+'>By : '+news.by+'</p>'+
                               '<p '+pStyle+'>Time : '+myDate.toUTCString()+'</p>'+
                            '</a></li>');
                       });

                       //now hide the loading the show the data
                       $("#loading").hide();
                       $("#dataDiv").show();
                }


        hackerNewsSdk.getTop20News(callBackFunction);
    }
};

app.initialize();