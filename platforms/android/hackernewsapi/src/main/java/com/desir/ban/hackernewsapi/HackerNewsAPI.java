package com.desir.ban.hackernewsapi;

import com.desir.ban.hackernewsapi.model.News;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by bandeshor on 3/17/2017.
 */

class HackerNewsAPI implements HackerNewsNewsGetter.NewsGetterInterface{

    private String BASE_URL = "https://hacker-news.firebaseio.com/v0/";
    private HackerNewsRetroInterface mHackerNewsRetroInterface = null;
    private HackerNewsAPIInterface hackerNewsAPIInterface;

    public void setHackerNewsAPIInterface(HackerNewsAPIInterface hackerNewsAPIInterface){
        this.hackerNewsAPIInterface = hackerNewsAPIInterface;
    }

    public void getTopNewsByNumber(int topX){
        RetroFitCaller retroFitCaller = new RetroFitCaller(BASE_URL);
        mHackerNewsRetroInterface = retroFitCaller.getRestInterface();
        getTopXNewsId(topX);
    }

    private void getTopXNewsId(final int topX){
        Call<ResponseBody> topNewsId =mHackerNewsRetroInterface.getTopNewsId();
        topNewsId.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String array = null;
                try {
                    array = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                convertAndReturnTopNews(array,topX);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void getListofTopXNews(int topX,String[] newsIds){
        HackerNewsNewsGetter hackerNewsNewsGetter = new HackerNewsNewsGetter(this);
        hackerNewsNewsGetter.getTopXNews(mHackerNewsRetroInterface,newsIds,topX);
    }

    private void convertAndReturnTopNews(String arrayString,int topX){
        if(arrayString == null)
            somethingWentWrongReturnEmptyList();
        arrayString = arrayString.replace("[","");
        arrayString = arrayString.trim();
        String[] ids = arrayString.split(",");
        getListofTopXNews(topX,ids);
    }

    private void somethingWentWrongReturnEmptyList(){
        if(hackerNewsAPIInterface!=null)
            hackerNewsAPIInterface.gotListTopXNews(new ArrayList<News>());
    }

    @Override
    public void gotAllNeeded(List<News> newsList) {
        if(hackerNewsAPIInterface!=null)
            hackerNewsAPIInterface.gotListTopXNews(newsList);
    }

    public interface HackerNewsAPIInterface{
        void gotListTopXNews(List<News> newsListws);
    }
}
