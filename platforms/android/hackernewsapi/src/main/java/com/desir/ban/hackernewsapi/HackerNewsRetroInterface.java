package com.desir.ban.hackernewsapi;

import com.desir.ban.hackernewsapi.model.News;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by bandeshor on 3/17/2017.
 */

interface HackerNewsRetroInterface {

    @GET("topstories.json")
    public Call<ResponseBody> getTopNewsId();

    @GET("item/{newsId}.json")
    public Call<News> getNewsFromId(@Path("newsId") String newsId);
}
