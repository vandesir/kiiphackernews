package com.desir.ban.hackernewsapi;

import com.desir.ban.hackernewsapi.model.News;

import java.util.List;

/**
 * Created by bandeshor on 3/18/2017.
 */

public class HackerNewsSDK{

    private HackerNewsAPI mHackerNewsAPI;
    private int TOP_X_NEWS = 20;
    private HackerNewsSDkInterface hackerNewsSDkInterface;

    public void initSDK(final HackerNewsSDkInterface hackerNewsSDkInterface){
        this.hackerNewsSDkInterface = hackerNewsSDkInterface;
        mHackerNewsAPI = new HackerNewsAPI();
        mHackerNewsAPI.setHackerNewsAPIInterface(new HackerNewsAPI.HackerNewsAPIInterface() {
            @Override
            public void gotListTopXNews(List<News> newsListws) {
                hackerNewsSDkInterface.gotTop20News(newsListws);
            }
        });
    }

    public void getTop20News(){
        mHackerNewsAPI.getTopNewsByNumber(TOP_X_NEWS);
    }

    public interface HackerNewsSDkInterface{
        void gotTop20News(List<News> newsList);
    }
}
