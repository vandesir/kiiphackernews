# README #

As Per the Gist https://gist.github.com/sraj/3766a23e41f47fdd712f66f852603cc2, I have created the application. There is a separate Library, hackernewapi, that works independently of the project and gives out the option initSDK and showHackerNews, which provides top 20 news. 

I have included the hackernewsapi as library/module in the project and created another cordova plugin that gets the the news sent and displays it and on click will redirect to the the news page with in App browser.

# Specification #
Uses Android Version 19 as base SDK and 25 as Target SDK both on the library and application, Can be set to lower if needed

# Use #
Used Android Studio to create the Application.

HackerNewApi is independent library and can be used in other application as added module, to import in project.